#!/bin/sh

python manage.py wait_for_db

python manage.py check --deploy
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:8000

#gunicorn -w 2 -k uvicorn.workers.UvicornH11Worker config.asgi:application --reload --bind 0.0.0.0:8000 --log-level info
#gunicorn config.asgi:application
# gunicorn config.wsgi:application --bind 0.0.0.0:8000 --reload
exec "$@"
