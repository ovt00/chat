from django.db import migrations
from django.contrib.auth.hashers import make_password
from django.conf import settings


def set_superuser(apps, schema_editor):
    group_obj = apps.get_model("auth", "Group")
    group = group_obj(
        name=settings.SUPPORT_GROUP_NAME
    )
    group.save()

    user_obj = apps.get_model("auth", "User")
    user = user_obj(
        email=settings.SUPERUSER_EMAIL,
        username=settings.SUPERUSER_USERNAME,
        first_name='Super',
        last_name='Admin',
        is_staff=True,
        is_active=True,
        is_superuser=True,
        password=make_password(settings.SUPERUSER_PASSWORD)
    )
    user.save()

    user.groups.add(group)


def reverse_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_auto_20210329_2217'),
    ]

    operations = [
        migrations.RunPython(set_superuser, reverse_func),
    ]
