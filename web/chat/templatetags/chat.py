from django import template
from django.conf import settings

register = template.Library()

title = settings.PROJECT_TITLE


@register.simple_tag
def project_title():
    return title
