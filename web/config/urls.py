# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import include
from django.contrib.auth.views import LoginView
from django.urls import path
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static
from chat.views import RegisterFormView, LogoutView, UsersListView

admin_url = settings.ADMIN_URL

urlpatterns = [
    path('auth/sign-in/', LoginView.as_view(template_name='login.html'), name='login'),
    path('auth/sign-up/', RegisterFormView.as_view(), name='sign_up'),
    path('auth/logout/', LogoutView.as_view(), name='logout'),

    path(f'{admin_url}/', admin.site.urls),

    path('', include('chat.urls', namespace='chat')),
    path('users/', UsersListView.as_view(), name='users_list'),
    path('', login_required(TemplateView.as_view(template_name='base.html')), name='home'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
