# Django app in docker with docker-compose

#### Project features:
* Docker-compose
* Setting Django app via environment variables
* Separated settings for Dev and Prod django version
* Docker configuration for nginx for 80 and/or 443 ports
* Redis service for websocket.
* Local mode (PyCharm Professional)
* ASGI support for dev and prod
* Flake8 integration
* Swagger in Django Admin Panel
* Ready for deploy by one click
* Separated configuration for dev and prod (requirements and settings)

#### Clone the repo:

    git clone https://ovt00@bitbucket.org/ovt00/chat.git
    

#### Before running add your superuser email/password and project name in docker/prod/env/.data.env file

    SUPERUSER_EMAIL=example@email.com
    SUPERUSER_PASSWORD=secretp@ssword
    PROJECT_TITLE=MyProject

#### Run the local develop server:

    docker-compose -f local.yml up -d --build
    docker-compose logs -f
    
##### Server will bind 8000 port. You can get access to server by browser [http://localhost:8000]
